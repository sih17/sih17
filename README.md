# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###


#### Summary of set up
* Always work in a virtual environment, install virtual environment from `https://virtualenv.pypa.io/en/stable/` .
* Download virtual environment for Python3.x .
* To run the project, go to the root directory of the project run `virtualenv env`
* Then install activate the virtual environment, using `source env/bin/activate`
* Install the dependencies by `pip3 install -r requirements.txt`
* To run the server `python3 manage.py runserver`
* To deactivate the virtual environment `deactivate`
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###
* Always work in a branch.
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin 
* Mail at `mailbag.akshay@gmail.com`
* Other community or team contact