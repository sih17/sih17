from django.contrib import admin

from .models import AnalysisTest

# Register your models here.
admin.site.register(AnalysisTest)
